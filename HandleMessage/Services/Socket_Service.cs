using HandleMessage.Model;
using Newtonsoft.Json;

namespace HandleMessage.Services;

public class Socket_Service
{
    private static readonly SocketIOClient.SocketIO _client = new SocketIOClient.SocketIO("http://localhost:3000/");
    public static Queue queue = new Queue();
    public static readonly object queueLock = new object(); // Lock for thread safety
    public static async Task Receive_Message()
    {
        try
        {
            // var client = ;
            
            _client.On("data", async (response) =>
            {
                
                string text = response.GetValue<string>();
                // var message = JsonConvert.DeserializeObject<Message>(text);
                // Console.WriteLine($"{message.email}, {message.text}");
                // await Task.Delay(TimeSpan.FromSeconds(5));
                // await Send_Message(new Message()
                // {
                //     email = "test@gmail.com",
                //     text = "Send back"
                // }, _client);
                
                lock (queueLock)
                {
                    queue.Enqueue(text);
                }
            });
            await _client.ConnectAsync();
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }

    }

    public static async Task Send_Message(Message? message)
    {
        try
        {
            await _client.EmitAsync("data-receive-back", JsonConvert.SerializeObject(message));
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        }
    }
}